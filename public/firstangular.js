/**
 * Created by Michael on 2016/6/30.
 */
//IIRE immediately invoked function expression
(function () {
       var MyFirstApp = angular.module("MyFirstApp", []);
        //call angular to create an object. module name is "MyFirstApp"
        // [] is dependency

        //controller function, the first one is named as MyFirstxxxxx
        var MyFirstCtrl = function () {
            var ctrl = this;
            //meaning this function.
            
            //then start to attach property
            ctrl.message = "Message to appear here";
            ctrl.convertToUpperCase = function () {
                ctrl.message = ctrl.message.toUpperCase();
            };
            ctrl.convertToLowerCase = function () {
                ctrl.message = ctrl.message.toLowerCase();
            };
        };

        //green MyFirstCtrl is the name of the controller,
        //the MyFirstCtrl in [] is the actual controller, functional one.
        MyFirstApp.controller ("MyFirstCtrl", [MyFirstCtrl]);
    }
) ();
