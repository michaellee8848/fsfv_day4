/**
 * Created by Michael on 2016/6/30.
 */
var express = require("express");
var app = express();

// start of request processing pipeline

//app.use (middleware)
//examine behaviour of /time
app.use(function (req, res, next) {
    // equal to app["use"]
    if ((req.method == "GET") && (req.originalUrl == "/time")) {
        res.status(200);
        res.type('text/html');
        res.send("<h1> The current time is:" + new Date() + "</h1>");
    }
    else
        next();
});

app.get("/prop", function (req, res) {
    res.status(200);
    res.type('text/html');
    var propName = "";
    for (var prop in app)
        propName += "<li>" + prop + "</li>";
    //equal to: propName = propName + "<li>" + prop + "</li>"
    res.send(
        "<h1> list of all properties in app</h1>" +
        "<ul>" + propName + "</ul>");
});
//app.get("/prop", handleGet);
//a function handle another function.

// var fred = {
//     name: "fred",
//     "last name": "flint",
//     age: 50,
//     propName: "hello, i'm property name",
// };
//
// for (var propName in fred)
//     console.info("property name: %s, property value: ", propName, fred.propName);

//p is bound variable, it's defined locally
var hello = function (p) {
    console.info(p);
    return (p.toLocaleUpperCase());
};
// the above is equal to the following

//the makeGreetings is a function from another function, but with parameter "name" remembered
//"name" is a free variable, it's defined universally, since it's from outer bound.
// all free variable, get their value, and remembered
var makeGreetings = function (name) {
    var greet = function (){
        console.info ("hello %s", name);
    };
    return (greet);
};
var greetFred = makeGreetings("Fred");
//Fred is a free variable, it's captured and fixed in greetFred.

var expressStatic = express.static(__dirname + "/public");

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/bower_components"));

app.use(function (req, res) {
    console.info("File not found in public: %s", req.originalUrl);
    res.redirect("/error.html");
});
// end of request processing pipeline

//config port and start serv
    app.set("port",
        process.argv[2] || process.env.APP_PORT || 3000);

    app.listen(app.get("port"), function () {
        console.info("Application started on port %d", app.get("port"));
    });